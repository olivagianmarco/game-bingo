const express = require("express");
const app = express();
const cors = require('cors'); 
const morgan = require('morgan');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan('dev'));

app.use("/game", require("./routes/game"));

app.listen(3000);
console.log("Server on port", 3000);

module.exports = app;
