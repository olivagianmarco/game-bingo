const { Router } = require('express');
const { getNum, getCartillas } = require('./../controller/game');
const router = Router();

/**
 * get all Num Bingo
 */
router.post("/getNum", getNum );

/**
 * Get a specific cartillas
 */
router.get("/cartillas/:num", getCartillas);

 
 

module.exports = router;
