const getNum = (req, res)=> {
    try {
        const { data } = req.body; 
        if ( typeof data === 'string' ) {
            const dataFormated = JSON.parse(data)
        
            let num = randomNumber(0,75)
             
            while( dataFormated.includes(num) ){
                num = randomNumber(0,75)
            }
            return res.send({message: 'Consulta correcta', num }) 
        } else {
            return res.status(400).send({message: 'Data debe ser string', num }) 
        }
       
    } catch (error) {
        return res.status(400).send({message: 'NOT_FOUND'})  
    }
}
 

const getCartillas = (req, res)=> {
    try { 
        const { num } = req.params
        var result = [];
        var arr = [];
        for (let index = 0; index < num; index++) {
            while(arr.length < 27){
                var r = Math.floor(randomNumber(0,75) ) ;
                if(arr.indexOf(r) === -1) arr.push(r);
            } 
            result.push(arr);
            arr = []
        }
       
        
        return res.send({message: 'Consulta correcta', result })    
    } catch (error) {
        return res.status(400).send({message: 'NOT_FOUND'})     
    }
}


const randomNumber= (minimum, maximum)=>{
    return Math.round( Math.random() * (maximum - minimum) + minimum);
}





module.exports = { 
    getNum,
    getCartillas
}