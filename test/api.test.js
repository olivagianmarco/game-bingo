const request = require("supertest");

const app = require("../src/app");
 
/**
 * Testing game endpoint  
 */
describe("GET /game/cartillas/:num", () => {
  it("Respond with json containing primers", (done) => {
    request(app)
      .get("/users/U0001")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
 
/**
 * Testing POST users endpoint
 */
describe("POST /game/getNum", () => {
  it("respond with 201 created", (done) => {
    const data = {
      data: "[1,2,2,55,7,56,78]",
     
    };
    request(app)
      .post("/game/getNum")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("respond with 400 on bad request", (done) => {
    const data = {
      data:  [1,2,2,55,7,56,78] ,
    };
    request(app)
      .post("/game/getNum")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(400)
      .expect('"Error"')
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });
});
